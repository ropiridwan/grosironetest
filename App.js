import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import PokemonList from './src/screen/PokemonList';
import ViewPokemonDetail from './src/screen/ViewPokemonDetail';
import MyPokemonList from './src/screen/MyPokemonList';

const Stack = createNativeStackNavigator();

const App = ({navigation}) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="PokemonList">
        <Stack.Screen
          name="PokemonList"
          component={PokemonList}
          // options={{
          //   headerShown: false,
          // }}
        />
        <Stack.Screen
          name="My Pokemon"
          component={MyPokemonList}
          // options={{
          //   headerShown: false,
          // }}
        />
        <Stack.Screen
          name="Pokemon Detail"
          component={ViewPokemonDetail}
          // options={{
          //   headerShown: false,
          // }}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
