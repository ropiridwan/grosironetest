import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';

const ViewPokemonDetail = ({route}) => {
  const {pokemon} = route.params;
  const [isCaptured, setIsCaptured] = useState(false);
  const [pokemonSpecies, setPokemonSpecies] = useState([]);
  const [pokemonColor, setPokemonColor] = useState('black');
  const [pokemonCatch, setPokemonCatch] = useState(50);

  // console.log('pokemon', pokemon);

  useEffect(() => {
    getPokemonList();
  }, []);

  const getPokemonList = async () => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon-species/${pokemon.species.name}/`)
      .then(async response => {
        await setPokemonSpecies(response.data);
        setPokemonColor(response.data?.color?.name);
        setPokemonCatch(response.data?.capture_rate);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleCapturePress = async () => {
    // const randomNumber = Math.floor(Math.random() * 2);
    // setIsCaptured(randomNumber === 0);
    if (pokemonCatch > 50) {
      let data = [];
      const getItem = await AsyncStorage.getItem('MyPokemonData');
      if (getItem == null) {
        setIsCaptured(true);
        data = [
          {
            myPokemon: pokemon,
          },
        ];
      } else {
        data = JSON.parse(getItem);
        if (data.some(item => item.myPokemon.name == pokemon.name)) {
          setIsCaptured(false);
          Alert.alert(
            'Anda Sudah Memiliki Pokemon Ini, Silahkan Pilih Pokemon Lain',
          );
        } else {
          setIsCaptured(true);
          data.unshift({
            myPokemon: pokemon,
          });
        }
      }
      const jsonValue = JSON.stringify(data);
      await AsyncStorage.setItem('MyPokemonData', jsonValue);
    } else {
      setIsCaptured(false);
      Alert.alert(
        'Tidak dapat menangkap pokemon karena presentase yang terlalu tinggi',
      );
    }
  };

  const renderPokemonType = ({item}) => (
    <View style={Styles.containerType}>
      <Text style={Styles.text}>
        Pokemon Type : {item.type.name.toUpperCase()}
      </Text>
    </View>
  );

  // console.log('pokemonSpecies ', JSON.stringify(pokemonSpecies));
  return (
    <View style={Styles.container}>
      <View style={Styles.containerName}>
        <Text style={Styles.text}>
          Pokemon Name :{' '}
          <Text style={{color: pokemonColor, fontWeight: '700'}}>
            {pokemon.name.toUpperCase()}
          </Text>
        </Text>
      </View>
      <FlatList
        // contentContainerStyle={{backgroundColor:'red', paddingVertical:4}}
        data={pokemon.types}
        renderItem={renderPokemonType}
        keyExtractor={item => item.slot}
      />
      <View style={Styles.containerName}>
        <Text style={Styles.text}>
          Pokemon Happines :{' '}
          <Text
            style={{
              color:
                pokemonSpecies.base_happiness == 50
                  ? 'orange'
                  : pokemonSpecies.base_happiness > 50
                  ? 'green'
                  : 'red',
            }}>
            {pokemonSpecies.base_happiness}%
          </Text>
        </Text>
      </View>
      <View style={Styles.containerName}>
        <Text style={Styles.text}>
          Persentage To Catch :{' '}
          <Text
            style={{
              color:
                pokemonCatch == 50
                  ? 'orange'
                  : pokemonCatch > 50
                  ? 'green'
                  : 'red',
            }}>
            {pokemonCatch}%
          </Text>
        </Text>
      </View>
      <View style={Styles.containeImages}>
        <Image
          source={{uri: pokemon.sprites.front_default}}
          resizeMode={'contain'}
          style={Styles.image}
        />
        {!isCaptured && (
          <TouchableOpacity style={Styles.button} onPress={handleCapturePress}>
            <Text style={Styles.textColor}>Tangkap Pokemon</Text>
          </TouchableOpacity>
          // <Button title="Tangkap Pokemon" onPress={handleCapturePress} />
        )}
        {isCaptured && (
          <Text style={[Styles.textCenter, Styles.text, {alignSelf: 'center'}]}>
            Selamat! Anda berhasil menangkap {pokemon.name}.
          </Text>
        )}
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  containerType: {
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 4,
  },
  container: {paddingHorizontal: 16, paddingVertical: 8},
  containeImages: {
    borderColor: 'black',
    borderWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 4,
  },
  image: {width: 100, height: 100, alignSelf: 'center'},
  button: {
    borderColor: 'blue',
    borderWidth: 1,
    paddingVertical: 8,
    marginHorizontal: 16,
    borderRadius: 24,
    backgroundColor: 'blue',
    marginTop: 16,
    alignItems: 'center',
  },
  containerName: {
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 4,
  },
  textColor: {color: 'white'},
  textCenter: {textAlign: 'center'},
  text: {color: 'black'},
});

export default ViewPokemonDetail;
