import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const PokemonList = ({navigation}) => {
  const [pokemonListData, setPokemonList] = useState([]);
  const [currenPage, setCurrenPage] = useState(20);
  const [isLoading, setIsloading] = useState(false);

  useEffect(() => {
    getPokemonList();
  }, [currenPage]);

  const getPokemonList = () => {
    setIsloading(true);
    axios
      .get(`https://pokeapi.co/api/v2/pokemon?limit=20&offset=${currenPage}`)
      .then(response => {
        setPokemonList([...pokemonListData, ...response.data.results]);
        setIsloading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handlePokemonPress = pokemon => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
      .then(response => {
        navigation.navigate('Pokemon Detail', {pokemon: response.data});
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleMyPokemonListPress = async () => {
    const data = await AsyncStorage.getItem('MyPokemonData');
    const value = JSON.parse(data);
    navigation.navigate('My Pokemon', {pokemonList: value});
  };

  const renderPokemonItem = ({item}) => (
    <TouchableOpacity
      style={Styles.containerType}
      onPress={() => handlePokemonPress(item)}>
      <Text style={Styles.text}>{item.name.toUpperCase()}</Text>
    </TouchableOpacity>
  );

  const renderLoader = () =>
    isLoading && (
      <View style={Styles.containerLoader}>
        <ActivityIndicator size={'large'} color={'black'} />
      </View>
    );

  const loadMoreItem = () => {
    setCurrenPage(currenPage + 20);
  };

  return (
    <View style={Styles.container}>
      <FlatList
        data={pokemonListData}
        renderItem={renderPokemonItem}
        keyExtractor={item => item.url}
        ListFooterComponent={renderLoader}
        onEndReached={loadMoreItem}
        onEndReachedThreshold={0}
      />
      <TouchableOpacity
        style={Styles.button}
        onPress={() => handleMyPokemonListPress()}>
        <Text style={Styles.textColor}>Lihat Koleksi Pokemonmu</Text>
      </TouchableOpacity>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 16, marginBottom: 20},
  containerType: {
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 8,
    marginTop: 20,
    borderRadius: 16,
  },
  button: {
    borderColor: 'blue',
    borderWidth: 1,
    paddingVertical: 8,
    marginHorizontal: 16,
    borderRadius: 24,
    backgroundColor: 'blue',
    marginTop: 16,
    alignItems: 'center',
  },
  textColor: {color: 'white'},
  containerLoader: {marginBottom: 16},
  text: {color: 'black', fontWeight: '700'},
});

export default PokemonList;
