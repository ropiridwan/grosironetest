import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Platform,
} from 'react-native';

const MyPokemonList = ({route}) => {
  const {pokemonList} = route.params;
  const [pokemon, setPokemon] = useState(pokemonList);
  const [pokemonRelease, setPokemonRelease] = useState(true);

  useEffect(() => {
    if (!pokemon) {
      setPokemonRelease(false);
    }
  }, []);

  const renderPokemonType = ({item}) => {
    return (
      <View style={Styles.containerType}>
        <Text style={Styles.text}>
          Pokemon Type : {item.type.name.toUpperCase()}
        </Text>
      </View>
    );
  };

  const removeAllItem = async () => {
    try {
      await AsyncStorage.removeItem('MyPokemonData');
      setPokemon([]);
      setPokemonRelease(false);
    } catch (e) {
      Alert.alert(e);
    }
  };

  const rennderItem = ({item}) => {
    return (
      <View style={Styles.containerItem}>
        <View style={Styles.containerName}>
          <Text style={Styles.text}>
            Name Pokemon : {item.myPokemon.name.toUpperCase()}
          </Text>
        </View>
        <FlatList
          data={item.myPokemon.types}
          renderItem={renderPokemonType}
          keyExtractor={item => item.slot}
        />
        <View style={Styles.containeImages}>
          <Image
            source={{uri: item.myPokemon.sprites.front_default}}
            resizeMode={'contain'}
            style={Styles.image}
          />
        </View>
      </View>
    );
  };

  return (
    <View style={Styles.container}>
      {!pokemon || !pokemonRelease ? (
        <View style={Styles.containerEmpty}>
          <Text
            style={[
              Styles.alignText,
              Styles.text,
              {fontSize: Platform.OS === 'android' ? 18 : 16},
            ]}>
            Anda Belum Memiliki Pokemon, kembali untuk menangkap pokemon
          </Text>
        </View>
      ) : null}
      <FlatList data={pokemon} renderItem={rennderItem} />
      {pokemon && pokemonRelease ? (
        <TouchableOpacity style={Styles.button} onPress={() => removeAllItem()}>
          <Text style={Styles.colorText}>Release Semua Pokemon</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const Styles = StyleSheet.create({
  containerType: {
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 4,
    paddingHorizontal: 4,
  },
  containerItem: {marginTop: 20},
  container: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    flex: 1,
  },
  alignText: {textAlign: 'center', fontSize: 16},
  containerEmpty: {alignItems: 'center', alignSelf: 'center'},
  containeImages: {
    borderColor: 'black',
    borderWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 4,
  },
  image: {width: 100, height: 100, alignSelf: 'center'},
  button: {
    borderColor: 'blue',
    borderWidth: 1,
    paddingVertical: 8,
    marginHorizontal: 16,
    borderRadius: 24,
    backgroundColor: 'blue',
    marginBottom: 20,
    alignItems: 'center',
  },
  buttonRelease: {
    borderColor: 'red',
    borderWidth: 1,
    paddingVertical: 8,
    marginHorizontal: 16,
    borderRadius: 24,
    backgroundColor: 'red',
    alignItems: 'center',
  },
  containerName: {
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 4,
  },
  colorText: {color: 'white'},
  text: {color: 'black'},
});

export default MyPokemonList;
